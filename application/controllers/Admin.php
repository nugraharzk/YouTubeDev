<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('VideoModel');
        $this->load->model('UserModel');
        $this->load->model('KomenModel');
        $this->load->model('LikeModel');
        $this->load->model('MasterModel');
    }

	function index()
	{
        $data['vid'] = $this->VideoModel->getAllVideo();
        $data['user'] = $this->UserModel->getAllUser();
        $data['kom'] = $this->KomenModel->getAllKomen();
        $data['like'] = $this->LikeModel->getAllLike();
        $data['itube'] = $this->MasterModel->getAllMaster();

        $this->load->view('globalIn/headerAdmin');
        $this->load->view('admin', $data);
		$this->load->view('global/footer');
	}

    function delete($id){
        $this->MasterModel->deleteMasterByUser($id);
        $this->KomenModel->deleteKomenByUser($id);
        $this->likeModel->deleteLikeByUser($id);
        $this->VideoModel->deleteVideoByUser($id);

        $delete = $this->UserModel->deleteUser($id);
        redirect('Admin');
    }

    function deleteVideo($id){
        $this->MasterModel->deleteMasterByVideo($id);
        $this->KomenModel->deleteKomenByVideo($id);
        $this->LikeModel->deleteLikeByVideo($id);

        $delete = $this->VideoModel->deleteVideo($id);
        redirect('Admin');
    }

    function deleteKomen($id){
        $this->MasterModel->deleteMasterByKomen($id);

        $delete = $this->KomenModel->deleteKomenByKomen($id);
        redirect('Admin');
    }
    
}
