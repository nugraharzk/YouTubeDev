<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper(array('url','html','form'));
    $this->load->model('VideoModel');
    $this->load->model('UserModel');
    $this->load->model('KomenModel');
    $this->load->model('LikeModel');
    $this->load->model('MasterModel');
  }

  public function index() {
    $cari = $this->input->post('cari');
    if($cari == 'All'){
      $dat['search'] = $this->VideoModel->getAllVideo();
    }else if($cari == 'Berita' || $cari == 'Film' || $cari == 'Musik'){
      $dat['search'] = $this->VideoModel->getAllVideoKategori($cari);
    }else{
      $dat['search'] = $this->VideoModel->getBySearch($cari);
    }

    if ($_SESSION['logged_in']) {
      if($_SESSION['email'] == 'admin@gmail.com'){
        $this->load->view('globalIn/headerAdmin');
        $this->load->view('hasil', $dat);
        $this->load->view('global/footer');
      }else{
        $this->load->view('globalIn/header');
        $this->load->view('hasil', $dat);
        $this->load->view('global/footer');
        
      }
    }else{
        $this->load->view('global/header');
        $this->load->view('hasil', $dat);
        $this->load->view('global/footer');
    }
  }


}