<?php 
	
	/**
	* UserModel.php
	*Model utuk user
	*/
	class KomenModel extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function insertKomen($data, $id, $iduser, $date){
			$data = array(
		        'id_user'       => $iduser,
		        'id_video'     => $id,
		        'message'   => $data['message'],
		        'tanggal_post' => $date,
		    );
		    $this->db->insert('komentar', $data);
		}

		public function getKomen($id){
			$this->db->select('komentar.*, user.nama')->from('komentar, user');
			$this->db->where('komentar.id_user = user.id_user AND komentar.id_video =', $id);
			$this->db->order_by('komentar.tanggal_post', 'asc');
			
			$query = $this->db->get();

			return $query->result();
		}

		public function getAllKomen(){
			$this->db->select('komentar.*, user.nama, video.nama_video')->from('komentar, user, video');
			$this->db->where('komentar.id_user = user.id_user AND komentar.id_video = video.id_video');
			$query = $this->db->get();

			return $query->result();
		}

		public function deleteKomenByUser($id){
			$this->db->where('id_user', $id);
			$this->db->delete('komentar');
		}

		public function deleteKomenByVideo($id){
			$this->db->where('id_video', $id);
			$this->db->delete('komentar');
		}

		public function deleteKomenByKomentar($id){
			$this->db->where('id_komentar', $id);
			$this->db->delete('komentar');
		}
	}
 ?>