<?php 
	
	/**
	* UserModel.php
	*Model utuk user
	*/
	class LikeModel extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function getLike($id){
			$this->db->select('likes')->from('video');
			$this->db->where('id_video = '. $id);
			$query = $this->db->get()->result();

			$a = (int)$query['0']->likes;
			$a = $a + 1;

			$data = array('likes' => $a);
			$this->db->where('id_video', $id);
			$this->db->update('video', $data);
		}

		public function getAllLike(){
			$this->db->select('likes.*, user.nama, video.nama_video')->from('likes, user, video');
			$this->db->where('likes.id_user = user.id_user AND likes.id_video = video.id_video');
			$query = $this->db->get();

			return $query->result();
		}
		public function deleteLikeByUser($id){
			$this->db->where('id_user', $id);
			$this->db->delete('likes');
		}
		
		public function deleteLikeByVideo($id){
			$this->db->where('id_video', $id);
			$this->db->delete('likes');
		}
	}
 ?>