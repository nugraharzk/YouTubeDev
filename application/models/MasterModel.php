<?php 
	
	/**
	* UserModel.php
	*Model utuk user
	*/
	class MasterModel extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function getAllMaster(){
			$this->db->select('itube.*, user.nama, video.nama_video, komentar.message, video.likes')->from('itube, user, video, komentar');
			$this->db->where('itube.id_user = user.id_user AND itube.id_video = video.id_video AND itube.id_komentar = komentar.id_komentar');

			$query = $this->db->get();

			return $query->result();
		}

		public function deleteMasterByUser($id){
			$this->db->where('id_user', $id);
			$this->db->delete('itube');
		}

		public function deleteMaster2($id){
			$this->db->where('id_video', $id);
			$this->db->delete('itube');
		}
		
		public function deleteMaster3($id){
			$this->db->where('id_komentar', $id);
			$this->db->delete('itube');
		}
	}
 ?>