<?php 
	
	/**
	* UserModel.php
	*Model utuk user
	*/
	class UserModel extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function login($data)
		{
			$this->db->where('email', $data['email']);
			$this->db->where('password', $data['password']);
			$query = $this->db->get('user');
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return null;
			}
		}

		public function register($data){
			$this->db->where('email', $data['email']);
			$user = $this->db->get('user')->row();
			if($user == null){
				$this->db->insert('user', $data);
				return 1;
			}else{
				return 0;
			}
		}

		public function uploadData($url, $date, $id){
		    $nama = $this->input->post('nama');
		    $deskripsi = $this->input->post('deskripsi');
		    $kategori = $this->input->post('kategori');
		    $data = array(
		        'src'       => $url.'.mp4',
		        'nama_video'     => $nama,
		        'deskripsi'   => $deskripsi,
		        'kategori' => $kategori,
		        'tanggal' => $date,
		        'id_user' => $id
		    );
		    $this->db->insert('video', $data);
		    $id_video = $this->db->insert_id();

		    $like = array('id_user' => $id, 'id_video' => $id_video);
		    $this->db->insert('likes', $like);
		    $id_like = $this->db->insert_id();
		    $komen = array('id_user' => $id, 'id_video' => $id_video, 'message' => 'hahahaha');
		    $this->db->insert('komentar', $komen);
		    $id_komen = $this->db->insert_id();

		    $tube = array(
		    	'id_user' => $id,
		    	'id_video' => $id_video,
		    	'id_komentar' => $id_komen,
		    	'id_like' => $id_like
		    );
		    $this->db->insert('itube', $tube);
		}

		public function getUserByEmail($email){
			$this->db->select('*')->from('user');
			$this->db->where('user.email = ', $email);
			$query = $this->db->get();

			return $query->result();
		}

		public function getUserById($id){
			$this->db->select('itube.*, video.*, user.*, likes.*, komentar.*')->from('itube, video, user, likes, komentar');
			$this->db->where('itube.id_user = user.id_user AND komentar.id_komentar = itube.id_komentar AND itube.id_like = likes.id_like AND itube.id_video = video.id_video AND itube.id_video =', $id);

			$query = $this->db->get();
			return $query->row();
		}

		public function getAllUser(){
			$this->db->select('*')->from('user');
			$query = $this->db->get();

			return $query->result();
		}

		public function deleteUser($id){
			$this->db->where('id_user', $id);
			$this->db->delete('user');
		}
	}
 ?>