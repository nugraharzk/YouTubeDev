<?php 
	
	/**
	* UserModel.php
	*Model utuk user
	*/
	class VideoModel extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function getAllVideo(){
			$this->db->select('video.*, user.nama')->from('video, user');
			$this->db->where('video.id_user = user.id_user');

			$query = $this->db->get();

			return $query->result();
		}

		public function getAllVideoKategori($dat){
			$this->db->select('*')->from('video');
			$this->db->where('kategori =', $dat);
			$query = $this->db->get();

			return $query->result();
		}

		public function getVU(){
			$this->db->select('video.*, user.nama as user_nama')->from('video, user');
			$this->db->where('video.id_user = user.id_user');
			
			$query = $this->db->get();
			return $query->result();
		}

		public function getById($id){
			$this->db->select('itube.*, video.*, user.*, likes.*, komentar.*')->from('itube, video, user, likes, komentar');
			$this->db->where('itube.id_user = user.id_user AND komentar.id_komentar = itube.id_komentar AND itube.id_like = likes.id_like AND itube.id_video = video.id_video AND itube.id_video =', $id);

			$query = $this->db->get();
			return $query->row();
		}

		public function getBySearch($cari){
			$this->db->select('video.*, user.*')->from('video, user');
			$this->db->where("video.id_user = user.id_user AND video.nama_video LIKE '%".$cari."%'");
			$query = $this->db->get();

			return $query->result();
		}

		public function deleteVideoByUser($id){
			$this->db->where('id_user', $id);
			$this->db->delete('video');
		}

		public function deleteVideo($id){
			$this->db->where('id_video', $id);
			$this->db->delete('video');
		}
	}
 ?>